import React from "react";
import { fireEvent, render, waitForElement } from "@testing-library/react";
import * as UsersService from "../../api/UsersService/UsersService";
import UsersList from "./UsersList";

const users = [
  {
    id: 3,
    name: "Clementine Bauch",
    username: "Samantha"
  },
  {
    id: 4,
    name: "Patricia Lebsack",
    username: "Karianne"
  }
];

describe("UsersList", () => {
  const getUsersReponse = Promise.resolve({ data: users });
  UsersService.getUsers = jest.fn().mockResolvedValue(getUsersReponse);

  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it("should get users and display them", async () => {
    const { getByText, getAllByRole } = await render(<UsersList />);

    await waitForElement(() => getAllByRole("listitem"));

    expect(getByText(users[0].name)).toBeInTheDocument();
    expect(getByText(users[1].name)).toBeInTheDocument();
  });

  it("should update search text on input change", async () => {
    const { getByRole, queryByText } = await render(<UsersList />);

    const searchInput = await waitForElement(() => getByRole("textbox"));
    fireEvent.change(searchInput, { target: { value: "Patricia" } });

    expect(queryByText(users[0].name)).toBeNull();
    expect(queryByText(users[1].name)).toBeInTheDocument();
  });

  it("should display error if request failed", async () => {
    const failedResponse = Promise.reject();
    UsersService.getUsers = jest.fn().mockReturnValueOnce(failedResponse);
    const { getByText, getByRole } = await render(<UsersList />);

    await waitForElement(() => getByRole("heading"));

    expect(getByText("error", { exact: false })).toBeInTheDocument();
  });
});
