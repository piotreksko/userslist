import React, { useEffect, useState } from "react";
import { getUsers } from "../../api/UsersService/UsersService";
import UserItem from "../UserItem/UserItem";
import SearchField from "../SearchField/SearchField";
import "./UsersList.css";

interface User {
  id: number;
  name: string;
  username: string;
}

const UsersList: React.FC = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [error, setError] = useState<string>("");
  const [searchText, setSearchText] = useState<string>("");

  async function fetchUsers(): Promise<any> {
    try {
      const { data: fetchedUsers } = await getUsers();
      const mappedUsers = fetchedUsers.map(({ id, name, username }) => ({
        id,
        name,
        username
      }));

      setUsers(mappedUsers);
    } catch (fetchError) {
      console.log(fetchError);
      setError("error");
    }
  }

  useEffect(() => {
    fetchUsers();
  }, []);

  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const getFilteredUsers = () =>
    users.filter(({ name }) =>
      name.toLowerCase().includes(searchText.toLowerCase())
    );

  const finalUsers = searchText ? getFilteredUsers() : users;

  if (error) {
    return <div>An error has occured. Please try again later</div>;
  }

  return (
    <div className="main">
      <h1>Users list</h1>

      <SearchField searchText={searchText} onSearchChange={onSearchChange} />

      <ul className="list">
        {finalUsers.map(user => (
          <UserItem key={user.id} {...user} />
        ))}
      </ul>
    </div>
  );
};

export default UsersList;
