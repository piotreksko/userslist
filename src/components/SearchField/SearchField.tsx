import React from "react";
import PropTypes from "prop-types";
import "./SearchField.css";

interface Props {
  searchText: string;
  onSearchChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchField: React.FC<Props> = ({
  searchText,
  onSearchChange
}) => (
  <input
    className="search-field"
    type="text"
    value={searchText}
    onChange={onSearchChange}
    placeholder="Search by user name..."
  />
);

SearchField.propTypes = {
  searchText: PropTypes.string.isRequired,
  onSearchChange: PropTypes.func.isRequired
};

export default SearchField;
