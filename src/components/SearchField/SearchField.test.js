import React from "react";
import { fireEvent, render } from "@testing-library/react";
import SearchField from "./SearchField";

describe("SearchField", () => {
  const onSearchChangeMock = jest.fn();
  const testText = "test text";

  const exampleSearchProps = {
    searchText: testText,
    onSearchChange: onSearchChangeMock
  };

  const placeholderText = "Search by user name...";

  it("renders search text", () => {
    const { getByPlaceholderText } = render(
      <SearchField {...exampleSearchProps} />
    );
    const searchInput = getByPlaceholderText(placeholderText);

    expect(searchInput.value).toBe(testText);
  });

  it("calls onSearchChange function", () => {
    const newTestValue = "new test value";
    const { getByPlaceholderText } = render(
      <SearchField {...exampleSearchProps} />
    );
    const searchInput = getByPlaceholderText(placeholderText);

    fireEvent.change(searchInput, { target: { value: newTestValue } });

    expect(onSearchChangeMock).toHaveBeenCalled();
  });
});
