import React from "react";
import { render } from "@testing-library/react";
import UserItem from "./UserItem";

describe("UserItem", () => {
  const exampleUserProps = {
    id: 1,
    name: "Clementine Bauch",
    username: "Samantha"
  };

  it("renders user information", () => {
    const { getByText } = render(<UserItem {...exampleUserProps} />);

    Object.values(exampleUserProps).forEach(prop => {
      expect(getByText(String(prop), { exact: false })).toBeInTheDocument();
    });
  });
});
