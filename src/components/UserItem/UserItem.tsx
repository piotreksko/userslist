import React from "react";
import PropTypes from "prop-types";
import "./UserItem.css";

interface Props {
  id: number;
  name: string;
  username: string;
}

const UserItem: React.FC<Props> = ({ id, name, username }) => (
  <li className="list-item">
    <span className="grey-text">{id}.</span>{" "}
    <span className="name">{name}</span>{" "}
    <span className="username grey-text">@{username}</span>
  </li>
);

UserItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired
};

export default UserItem;
