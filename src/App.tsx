import React from "react";
import UsersList from "./components/UsersList/UsersList";

const App = () => {
  return <UsersList />;
};

export default App;
