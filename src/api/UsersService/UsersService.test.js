import axios from "axios";
import urls from "../../constants/urls";
import { getUsers } from "./UsersService";

jest.mock("axios");

describe("UsersService", () => {
  it("should get users", () => {
    getUsers();
    expect(axios.get).toHaveBeenCalledWith(urls.USERS);
  });
});
