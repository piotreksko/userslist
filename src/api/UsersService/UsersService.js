import axios from "axios";
import urls from "../../constants/urls";

function getUsers() {
  return axios.get(urls.USERS);
}

export { getUsers };
